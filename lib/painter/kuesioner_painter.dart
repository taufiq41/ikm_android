import 'package:flutter/material.dart';

class KuesionerPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    final paint2 = Paint();

    paint.color = Colors.yellow;
    paint2.color = Colors.yellow[300];


    var path2  = Path();
    path2.lineTo(size.width * 0.6, 0 );
    path2.lineTo(0, size.height * 0.7);
    path2.close();
    canvas.drawPath(path2, paint2);

    var path  = Path();
    path.lineTo(0, size.height * 0.5 );
    path.lineTo(size.width * 0.6, 0);
    path.close();
    canvas.drawPath(path, paint);

    var path4  = Path();
    path4.lineTo(size.width, size.height * 0.4);
    path4.lineTo(size.width * 0.4, size.height);
    path4.lineTo(size.width, size.height);
    path4.lineTo(size.width, size.height * 0.4);
    canvas.drawPath(path4, paint2);

    var path3  = Path();
    path3.lineTo(size.width, size.height);
    path3.lineTo(size.width, size.height * 0.4);
    path3.lineTo(size.width * 0.6, size.height);
    path3.lineTo(size.width, size.height);
    canvas.drawPath(path3, paint);

  }

  @override
  bool shouldRepaint(KuesionerPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(KuesionerPainter oldDelegate) => true;
}