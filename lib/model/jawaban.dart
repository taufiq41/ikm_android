class Jawaban{

  String id;
  String idPertanyaan;
  String nilai;
  
  Jawaban({this.id, this.idPertanyaan, this.nilai});

  factory Jawaban.fromJson(Map<String, dynamic> json) {
    return Jawaban(
      id : json['id'],
      idPertanyaan: json['pertanyaan_id'],
      nilai: json['nilai'],
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'pertanyaan_id': idPertanyaan,
      'nilai' : nilai,
    };
    return map;
  }
}