import 'penilaian.dart';

class Pertanyaan{

  String id, penilaianId, pertanyaan;
  Penilaian penilaian;
  
  Pertanyaan({this.id, this.pertanyaan, this.penilaianId});

  factory Pertanyaan.fromJson(Map<String, dynamic> json) {
    return Pertanyaan(
      id : json['id'],
      penilaianId: json['penilaian_id'],
      pertanyaan: json['pertanyaan'],
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'penilaian_id': penilaianId,
      'pertanyaan' : pertanyaan,
    };
    return map;
  }
}