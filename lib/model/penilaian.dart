class Penilaian{
  
  String id, target_penilaian, objek_penilaian, logo_objek_penilaian, skala, jenisJawaban;

  Penilaian({this.id, this.target_penilaian, this.objek_penilaian, this.logo_objek_penilaian, this.skala, this.jenisJawaban});

  factory Penilaian.fromJson(Map<String, dynamic> json) {
    return Penilaian(
      id : json['id'].toString(),
      target_penilaian: json['target_penilaian'],
      objek_penilaian: json['objek_penilaian'],
      logo_objek_penilaian: json['logo_objek_penilaian'],
      skala: json['skala'].toString(),
      jenisJawaban: json['jenis_jawaban'],
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'target_penilaian': target_penilaian,
      'objek_penilaian' : objek_penilaian,
      'logo_objek_penilaian' : logo_objek_penilaian,
      'skala' : skala,
      'jenis_jawaban' : jenisJawaban,
    };
    return map;
  }
}