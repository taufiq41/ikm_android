import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'model/jawaban.dart';
import 'database/jawaban_service.dart';
import 'model/data_chart.dart';

class Charts2 extends StatefulWidget {
  @override
  _Charts2State createState() => _Charts2State();
}

class _Charts2State extends State<Charts2> {

  List<charts.Series<DataChart, String>> _seriesData;
  String ipAddress;
  String idPenilaian, targetPenilaian, objekPenilaian, logoObjekPenilaian, skala, jenisJawaban, idPertanyaan, pertanyaan;

  JawabanService jawabanService;

  @override
  void initState() {
    jawabanService = JawabanService();
    
    SharedPreferences.getInstance().then((SharedPreferences prefs){
      idPenilaian = prefs.get('id_penilaian');
      targetPenilaian = prefs.get('target_penilaian');
      objekPenilaian = prefs.get('objek_penilaian');
      logoObjekPenilaian = prefs.get('logo_objek_penilaian');
      skala = prefs.get('skala');
      jenisJawaban = prefs.get('jenis_jawaban');
      idPertanyaan = prefs.get('id_pertanyaan');
      pertanyaan = prefs.get('pertanyaan');
      setState(() {
            
      });
    });
    super.initState();
  }

  Future<List<DataChart>> getJawaban() async{
    
    try{
      List<DataChart> list;

      await jawabanService.getJawabanById(idPertanyaan : idPertanyaan).then((Map<String, dynamic> data){
        
        
        print('coba cetak '+data['dua']);

        if(skala == '5'){
            
            if(data['lima'] != null){
              list = [
                  DataChart(domain : 'Sangat '+jenisJawaban, jumlah: int.parse(data['lima'])),
                  DataChart(domain : jenisJawaban, jumlah: int.parse(data['empat'])),
                  DataChart(domain : 'Netral', jumlah: int.parse(data['tiga'])),
                  DataChart(domain : 'Tidak '+jenisJawaban, jumlah: int.parse(data['dua'])),
                  DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: int.parse(data['satu'])),
              ];
            }else{
              list = [
                  DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
                  DataChart(domain : jenisJawaban, jumlah: 0),
                  DataChart(domain : 'Netral', jumlah: 0),
                  DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
                  DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
              ];
            }
            
        }else if(skala == '3'){

          if(data['tiga'] != null){
            print('Cetak '+data['tiga']+' '+data['dua']+' '+data['satu']);
            list = [
              DataChart(domain : jenisJawaban, jumlah: int.parse(data['tiga'])),
              DataChart(domain : 'Netral', jumlah: int.parse(data['dua'])),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: int.parse(data['satu'])),
            ];
          }else{
            list = [
              DataChart(domain : jenisJawaban, jumlah: 0),
              DataChart(domain : 'Netral', jumlah: 0),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
            ];
          }
          
        }else if(skala == '2'){
          
          if(data['dua'] != null){
            list = [
              DataChart(domain : jenisJawaban, jumlah: int.parse(data['satu'])),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: int.parse(data['nol'])),
            ];
          }else{
            list = [
              DataChart(domain : jenisJawaban, jumlah: 0),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
            ];
          }
          
        }else{
          list = [
              DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
              DataChart(domain : jenisJawaban, jumlah: 0),
              DataChart(domain : 'Netral', jumlah: 0),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
              DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
          ];
        }
        print('sebelum return');
      }).catchError((Object ex){
        print('error guys '+ex.toString());
        
      }).whenComplete((){
        print('selesai bro');
      });

      return list;
    }catch(ex){
      print('Kesalahan charts '+ex.toString());
      if(skala == '5'){
        return [
          DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
          DataChart(domain : jenisJawaban, jumlah: 0),
          DataChart(domain : 'Netral', jumlah: 0),
          DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
          DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
        ];
      }else if(skala == '3'){
        return [
          DataChart(domain : jenisJawaban, jumlah: 0),
          DataChart(domain : 'Netral', jumlah: 0),
          DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
        ];
      }else if(skala == 2){
        return [
          DataChart(domain : jenisJawaban, jumlah: 0),
          DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
        ];
      }
    }

  }

  @override
  Widget build(BuildContext context) {

    Timer(const Duration(milliseconds: 4000), () {
      Navigator.of(context).pushNamedAndRemoveUntil('/kuesioner', (Route<dynamic> route) => false);
    });
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(50),
      child: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder(
                future: getJawaban(),
                builder: (BuildContext context, AsyncSnapshot<List<DataChart>> snapshot){
                  
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting : 
                      return Center(
                        child: CircularProgressIndicator()
                      );
                    case ConnectionState.done :
                      
                      if(snapshot.hasData){
                        _seriesData = List<charts.Series<DataChart, String>>();
                        _seriesData.add(
                          charts.Series(
                            domainFn: (DataChart dataChart, _) => dataChart.domain,
                            measureFn: (DataChart dataChart, _) => dataChart.jumlah,
                            id: 'Grafik',
                            data: snapshot.data,
                            fillPatternFn: (_, __) => charts.FillPatternType.solid,
                            fillColorFn: (DataChart dataChart, _) =>
                                charts.ColorUtil.fromDartColor(Colors.blueAccent),
                          ), 
                        );
                        return charts.BarChart(
                          _seriesData,
                          animate: true,
                          barGroupingType: charts.BarGroupingType.grouped,
                          animationDuration: Duration(seconds: 1),
                        );
                      }else{
                        return Center(child: Text('Menunggu2...'),);
                      }
                      break;
                    default:
                      return Center(child: Text('Menunggu3...'),);
                  }
                },
              )
            )
          ],
        ),
      ),
    );
  }
}