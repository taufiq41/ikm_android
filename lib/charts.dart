import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class Charts extends StatefulWidget {
  @override
  _ChartsState createState() => _ChartsState();
}

class _ChartsState extends State<Charts> {

  List<charts.Series<DataChart, String>> _seriesData;
  String ipAddress;
  String idPenilaian, targetPenilaian, objekPenilaian, logoObjekPenilaian, skala, jenisJawaban, idPertanyaan, pertanyaan;

  Future<List<DataChart>> getJawaban() async{
    print('get jawaban');
    print('id_pertanyaan '+idPertanyaan);
    try{
      var jsonEncode = json.encode({'pertanyaan_id' : idPertanyaan});

      final response = await http.post(
        'http://'+ipAddress+'/ikm/api/get_jawaban.php',
        body : jsonEncode, 
        headers: {'Accept' : 'application/json'}
      );

      print(response.statusCode);
      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        List<DataChart> list;
        
        print(data);
        if(skala == '5'){
            if(data['lima'] != null){
              list = [
                  DataChart(domain : 'Sangat '+jenisJawaban, jumlah: int.parse(data['lima'])),
                  DataChart(domain : jenisJawaban, jumlah: int.parse(data['empat'])),
                  DataChart(domain : 'Netral', jumlah: int.parse(data['tiga'])),
                  DataChart(domain : 'Tidak '+jenisJawaban, jumlah: int.parse(data['dua'])),
                  DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: int.parse(data['satu'])),
              ];
            }else{
              list = [
                  DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
                  DataChart(domain : jenisJawaban, jumlah: 0),
                  DataChart(domain : 'Netral', jumlah: 0),
                  DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
                  DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
              ];
            }
           
        }else if(skala == '3'){

          if(data['tiga'] != null){
            print('Cetak '+data['tiga']+' '+data['dua']+' '+data['satu']);
            list = [
              DataChart(domain : jenisJawaban, jumlah: int.parse(data['tiga'])),
              DataChart(domain : 'Netral', jumlah: int.parse(data['dua'])),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: int.parse(data['satu'])),
            ];
          }else{
            print('Cetak 0 0 0');
            list = [
              DataChart(domain : jenisJawaban, jumlah: 0),
              DataChart(domain : 'Netral', jumlah: 0),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
            ];
          }
          
        }else if(skala == '2'){
          if(data['dua'] != null){
            list = [
              DataChart(domain : jenisJawaban, jumlah: int.parse(data['satu'])),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: int.parse(data['nol'])),
            ];
          }else{
            list = [
              DataChart(domain : jenisJawaban, jumlah: 0),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
            ];
          }
          
        }else{
          list = [
              DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
              DataChart(domain : jenisJawaban, jumlah: 0),
              DataChart(domain : 'Netral', jumlah: 0),
              DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
              DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
          ];
        }

        return list;

      }else{
        print('Kesalahan API');
        
        if(skala == '5'){
          return [
            DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
            DataChart(domain : jenisJawaban, jumlah: 0),
            DataChart(domain : 'Netral', jumlah: 0),
            DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
            DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
          ];
        }else if(skala == '3'){
          return [
            DataChart(domain : jenisJawaban, jumlah: 0),
            DataChart(domain : 'Netral', jumlah: 0),
            DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
          ];
        }else if(skala == 2){
          return [
            DataChart(domain : jenisJawaban, jumlah: 0),
            DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
          ];
        }
        
      }

    }catch(ex){
      print('Kesalahan charts '+ex.toString());
      if(skala == '5'){
        return [
          DataChart(domain : 'Sangat '+jenisJawaban, jumlah: 0),
          DataChart(domain : jenisJawaban, jumlah: 0),
          DataChart(domain : 'Netral', jumlah: 0),
          DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
          DataChart(domain : 'Sangat Tidak '+jenisJawaban, jumlah: 0),
        ];
      }else if(skala == '3'){
        return [
          DataChart(domain : jenisJawaban, jumlah: 0),
          DataChart(domain : 'Netral', jumlah: 0),
          DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
        ];
      }else if(skala == 2){
        return [
          DataChart(domain : jenisJawaban, jumlah: 0),
          DataChart(domain : 'Tidak '+jenisJawaban, jumlah: 0),
        ];
      }
    }

  }

 

  @override
  void initState() {
    print('running charts');
    SharedPreferences.getInstance().then((SharedPreferences prefs){
      ipAddress = prefs.get('ip_address');
      idPenilaian = prefs.get('id_penilaian');
      targetPenilaian = prefs.get('target_penilaian');
      objekPenilaian = prefs.get('objek_penilaian');
      logoObjekPenilaian = prefs.get('logo_objek_penilaian');
      skala = prefs.get('skala');
      jenisJawaban = prefs.get('jenis_jawaban');
      idPertanyaan = prefs.get('id_pertanyaan');
      pertanyaan = prefs.get('pertanyaan');
      setState(() {
           
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    /* WidgetsBinding.instance.addPostFrameCallback((_) => {
      Navigator.of(context).pushNamedAndRemoveUntil('/penilaian', (Route<dynamic> route) => false)
    }); */
    Timer(const Duration(milliseconds: 2500), () {
      Navigator.of(context).pushNamedAndRemoveUntil('/penilaian', (Route<dynamic> route) => false);
    });
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(50),
      child: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder(
                future: getJawaban(),
                builder: (BuildContext context, AsyncSnapshot<List<DataChart>> snapshot){
                  _seriesData = List<charts.Series<DataChart, String>>();
                  _seriesData.add(
                    charts.Series(
                      domainFn: (DataChart dataChart, _) => dataChart.domain,
                      measureFn: (DataChart dataChart, _) => dataChart.jumlah,
                      id: 'Grafik',
                      data: snapshot.data,
                      fillPatternFn: (_, __) => charts.FillPatternType.solid,
                      fillColorFn: (DataChart dataChart, _) =>
                          charts.ColorUtil.fromDartColor(Colors.blueAccent),
                    ), 
                  );
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting : 
                      return Center(
                        child: CircularProgressIndicator()
                      );
                    case ConnectionState.done :
                      return charts.BarChart(
                        _seriesData,
                        animate: true,
                        barGroupingType: charts.BarGroupingType.grouped,
                        animationDuration: Duration(seconds: 1),
                      );
                      
                    default:

                  }
                },
              )
            )
          ],
        ),
      ),
    );
  }
}

class DataChart{

   int jumlah;
   String domain;

   DataChart({this.jumlah, this.domain});
}