import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:rflutter_alert/rflutter_alert.dart';

class HomeSkala5 extends StatefulWidget {
  HomeSkala5({Key key}) : super(key: key);
  
  _HomeSkala5State createState() => _HomeSkala5State();
}

class _HomeSkala5State extends State<HomeSkala5> {

  String idPenilaian, targetPenilaian, objekPenilaian, logoObjekPenilaian, skala, jenisJawaban, idPertanyaan, pertanyaan;
  String ipAddress;
  
  @override
  void initState(){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    SystemChrome.setEnabledSystemUIOverlays([]);
    SharedPreferences.getInstance().then((SharedPreferences prefs){
      ipAddress = prefs.get('ip_address');
      idPenilaian = prefs.get('id_penilaian');
      targetPenilaian = prefs.get('target_penilaian');
      objekPenilaian = prefs.get('objek_penilaian');
      logoObjekPenilaian = prefs.get('logo_objek_penilaian');
      skala = prefs.get('skala');
      jenisJawaban = prefs.get('jenis_jawaban');
      idPertanyaan = prefs.get('id_pertanyaan');
      pertanyaan = prefs.get('pertanyaan');
      setState(() {
        
           
      });
    });
    super.initState();
  }

  Color color0 = Colors.yellow;
  Color color1 = Colors.yellow;
  Color color2 = Colors.yellow;
  Color color3 = Colors.yellow;
  Color color4 = Colors.yellow;
  Color color5 = Colors.yellow;
  int nilai = 0;
  
  @override
  Widget build(BuildContext context) {
    
    Size size = MediaQuery.of(context).size;
    double width = 0;
    double height = 0;
    
    if(skala == '5'){
      width = size.width/5 - 11;
      height = size.height/3;
    }else if(skala == '3'){
      width = size.width/5 - 11;
      height = size.height/3;
    }else if(skala == '2'){
      width = size.width/5 - 11;
      height = size.height/3;
    } 

    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.black12,
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 25,),
                Text(
                  objekPenilaian != null ? objekPenilaian : '',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blueAccent),
                ),
                SizedBox(height: 20,),
                Text(
                  pertanyaan != null ? pertanyaan : '',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blueAccent),
                ),
                SizedBox(height: 30,),

                skala == '5' ?
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    pilihan(context, 'Sangat '+jenisJawaban , 5, width, height, 'assets/happy.png'),
                    pilihan(context, jenisJawaban, 4, width, height, 'assets/smiling.png'),
                    pilihan(context, 'Netral', 3, width, height, 'assets/confused.png'),
                    pilihan(context, 'Tidak '+jenisJawaban, 2, width, height, 'assets/sad.png'),
                    pilihan(context, 'Sangat Tidak '+jenisJawaban, 1, width, height, 'assets/unhappy.png'),
                  ],
                )
                :
                skala == '3' ?
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    pilihan(context, 'Sangat '+jenisJawaban , 3, width, height,'assets/happy.png'), 
                    pilihan(context, 'Netral', 2, width, height, 'assets/confused.png'),
                    pilihan(context, 'Tidak '+jenisJawaban, 1, width, height, 'assets/sad.png'),
                  ],
                )
                :
                skala == '2' ?
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    pilihan(context, 'Sangat '+jenisJawaban , 1, width, height, 'assets/happy.png'),
                    
                    pilihan(context, 'Tidak '+jenisJawaban, 0, width, height, 'assets/sad.png'),
                    
                  ],
                )
                :
                
                SizedBox(height: 20,),
                SizedBox(height: 20,),
                SizedBox(
                  width: 100,
                  height: 35,
                  child: RaisedButton(
                  
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Submit', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),),
                    onPressed: _submit,
                    color: Colors.blueAccent,
                  ),
                )
              ],
            )
          ),
        ),
      )
    );
  
  }

  Widget skala5(BuildContext context){
    print('jalur skala 5');
    
    Size size = MediaQuery.of(context).size;
    double width = size.width/5 - 11;
    double height = size.height/3;

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 25,),
              Text(
                objekPenilaian,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blueAccent),
              ),
              SizedBox(height: 20,),
              Text(
                pertanyaan,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blueAccent),
              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            color1 = Colors.yellow;
                            color2 = Colors.yellow;
                            color3 = Colors.yellow;
                            color4 = Colors.yellow;
                            color5 = Colors.tealAccent[700];
                            nilai = 5;
                          });
                        },
                        child: Container(
                          width: width,
                          height: height,
                          child: Image.asset('assets/happy.png'),
                          decoration: BoxDecoration(
                            color: color5,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.blueAccent, width: 5)
                          ),
                        ),
                      ),
                      Text('Sangat Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            color1 = Colors.yellow;
                            color2 = Colors.yellow;
                            color3 = Colors.yellow;
                            color4 = Colors.tealAccent[700];
                            color5 = Colors.yellow; 
                            nilai = 4;
                          });
                        },
                        child: Container(
                          width: width,
                          height: height,
                          child: Image.asset('assets/smiling.png'),
                          decoration: BoxDecoration(
                            color: color4,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.blueAccent, width: 5)
                          ),
                        ),
                      ),
                      Text('Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            color1 = Colors.yellow;
                            color2 = Colors.yellow;
                            color3 = Colors.tealAccent[700];
                            color4 = Colors.yellow;
                            color5 = Colors.yellow; 
                            nilai = 3;
                          });
                        },
                        child: Container(
                          width: width,
                          height: height,
                          child: Image.asset('assets/confused.png'),
                          decoration: BoxDecoration(
                            color: color3,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.blueAccent, width: 5)
                          ),
                        ),
                      ),
                      Text('Netral', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            color1 = Colors.yellow;
                            color2 = Colors.tealAccent[700];
                            color3 = Colors.yellow;
                            color4 = Colors.yellow;
                            color5 = Colors.yellow; 
                            nilai = 2;
                          });
                        },
                        child: Container(
                          width: width,
                          height: height,
                          child: Image.asset('assets/unhappy.png'),
                          decoration: BoxDecoration(
                            color: color2,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.blueAccent, width: 5)
                          ),
                        ),
                      ),
                      Text('Tidak Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          setState(() {
                            color1 = Colors.tealAccent[700];
                            color2 = Colors.yellow;
                            color3 = Colors.yellow;
                            color4 = Colors.yellow;
                            color5 = Colors.yellow; 
                            nilai = 1;
                          });
                        },
                        child: Container(
                          width: width,
                          height: height,
                          child: Image.asset('assets/sad.png'),
                          decoration: BoxDecoration(
                            color: color1,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: Border.all(color: Colors.blueAccent, width: 5)
                          ),
                        ),
                      ),
                      Text('Sangat Tidak Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                    ],
                  ),
                ],
              ),
              SizedBox(height: 20,),
              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Text('Submit', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                onPressed: _submit,
                color: Colors.blueAccent,
              ),
            ],
          )
        ),
      )
    );
  }

  Widget skala3(BuildContext context){
    print('jalur skala 3');
    Size size = MediaQuery.of(context).size;
    double width = size.width/5 - 11;
    double height = size.height/3;
    
    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 25,),
            Text(
              objekPenilaian,
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blueAccent),
            ),
            SizedBox(height: 20,),
            Text(
              pertanyaan,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blueAccent),
            ),
            SizedBox(height: 30,),
            Container(
              margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                color1 = Colors.tealAccent[700];
                                color2 = Colors.yellow;
                                color3 = Colors.yellow;
                                nilai = 3;
                              });
                            },
                            child: Container(
                              width: width,
                              height: height,
                              child: Image.asset('assets/happy.png'),
                              decoration: BoxDecoration(
                                color: color1,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: Colors.blueAccent, width: 5)
                              ),
                            ),
                          ),
                          Text('Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                color1 = Colors.yellow;
                                color2 = Colors.tealAccent[700];
                                color3 = Colors.yellow;
                                nilai = 2;
                              });
                            },
                            child: Container(
                              width: width,
                              height: height,
                              child: Image.asset('assets/confused.png'),
                              decoration: BoxDecoration(
                                color: color2,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: Colors.blueAccent, width: 5)
                              ),
                            ),
                          ),
                          Text('Cukup', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                color1 = Colors.yellow;
                                color2 = Colors.yellow;
                                color3 = Colors.tealAccent[700];
                                nilai = 1;
                              });
                            },
                            child: Container(
                              width: width,
                              height: height,
                              child: Image.asset('assets/unhappy.png'),
                              decoration: BoxDecoration(
                                color: color3,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: Colors.blueAccent, width: 5)
                              ),
                            ),
                          ),
                          Text('Tidak Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Submit', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                    onPressed: _submit,
                    color: Colors.blueAccent,
                  ),
                ],
              ),
            )
          ],
        )
      ),
    );
  }

  Widget skala2(BuildContext context){
    print('jalur skala 2');
    Size size = MediaQuery.of(context).size;
    double width = size.width/5 - 11;
    double height = size.height/3;

    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 25,),
            Text(
              objekPenilaian,
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.blueAccent),
            ),
            SizedBox(height: 20,),
            Text(
              pertanyaan,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blueAccent),
            ),
            SizedBox(height: 30,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          color1 = Colors.tealAccent[700];
                          color2 = Colors.yellow;
                          nilai = 1;
                        });
                      },
                      child: Container(
                        width: width,
                        height: height,
                        child: Image.asset('assets/happy.png'),
                        decoration: BoxDecoration(
                          color: color1,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: Colors.blueAccent, width: 5)
                        ),
                      ),
                    ),
                    Text('Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                  ],
                ),
                Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        setState(() {
                          color1 = Colors.yellow;
                          color2 = Colors.tealAccent[700];
                          nilai = 0;
                        });
                      },
                      child: Container(
                        width: width,
                        height: height,
                        child: Image.asset('assets/unhappy.png'),
                        decoration: BoxDecoration(
                          color: color2,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: Colors.blueAccent, width: 5)
                        ),
                      ),
                    ),
                    Text('Tidak Puas', style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
                  ],
                ),
              ],
            ),
            SizedBox(height: 20,),
            RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text('Submit', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
              onPressed: _submit,
              color: Colors.blueAccent,
            ),
          ],
        )
      ),
    );
  }

  Widget pilihan(BuildContext context, String keterangan, int _nilai, double width, double height, String images){

    return 
    Column(
      children: <Widget>[
        GestureDetector(
          
          onTap: (){
            setState(() {
              color0 = _nilai == 0 ? Colors.tealAccent[700] : Colors.yellow;
              color1 = _nilai == 1 ? Colors.tealAccent[700] : Colors.yellow;
              color2 = _nilai == 2 ? Colors.tealAccent[700] : Colors.yellow;
              color3 = _nilai == 3 ? Colors.tealAccent[700] : Colors.yellow;
              color4 = _nilai == 4 ? Colors.tealAccent[700] : Colors.yellow;
              color5 = _nilai == 5 ? Colors.tealAccent[700] : Colors.yellow;
              nilai = _nilai;
            });
          },
          child: Container(
            width: width,
            height: height,
            child: Image.asset(images),
            decoration: BoxDecoration(
              color: _nilai == 5 ? color5 : _nilai == 4 ? color4 : _nilai == 3 ? color3 : _nilai == 2 ? color2 : _nilai == 1 ? color1 : color0,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: Colors.blueAccent, width: 5)
            ),
          ),
        ),
        Text(keterangan, style: TextStyle(color: Colors.blueAccent, fontSize: 17, fontWeight: FontWeight.bold),)
      ],
    );
  }
  
  void _submit() async{
    
    try{
        var jsonEncode = json.encode({'pertanyaan_id' : idPertanyaan, 'nilai' : nilai});
        final response = await http.post(
          'http://'+ipAddress+'/ikm/api/post_jawaban.php',
          body: jsonEncode,
          headers: {'Accept' : 'application/json'}
        );

        if (response.statusCode == 200) {
          var data = jsonDecode(response.body);
          print(data['result']);

          Navigator.of(context).pushNamed('/charts');

        }else{
          Alert(context: context, title: "Kesalahan", desc: "Kesalahan pada acess API").show();
        }

    }catch(ex){
      print('Kesalahan '+ex.toString());
    }   
  }
}