import 'dart:io';
import 'Modal.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'painter/default_painter.dart';
import 'painter/penilaian_painter.dart';
import 'pages/edit_penilaian.dart';
import 'package:flutter/services.dart';
import 'model/penilaian.dart';
import 'model/pertanyaan.dart';
import 'database/penilaian_service.dart';
import 'database/pertanyaan_service.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SetupPertanyaanOffline extends StatefulWidget {
  @override
  _SetupPertanyaanOfflineState createState() => _SetupPertanyaanOfflineState();
}

class _SetupPertanyaanOfflineState extends State<SetupPertanyaanOffline> {

  int _currentIndex;
  Penilaian _currentPenilaian;
  Pertanyaan _currentPertanyaan;
  Future<List<Penilaian>> listPenilaian;
  Future<List<Pertanyaan>> listPertanyaan;
  Future<List<Pertanyaan>> listPertanyaanByIdPenilaian;
  String idPenilaian;
  PenilaianService penilaianService;
  PertanyaanService pertanyaanService;
  final _formKey = GlobalKey<FormState>();
  String idPenilaianEditDelete;
  String pencarian;
  String idPenilaianSession;
  String idPertanyaanSession;

  @override
  void initState() {
    _currentIndex = 1;
    idPenilaian = '0';
    idPenilaianEditDelete = '0';
    setState(() {

      penilaianService = PenilaianService(); 
      pertanyaanService =PertanyaanService();
     
      listPenilaian = penilaianService.getAllPenilaian();
      listPertanyaan = pertanyaanService.getAllPertanyaanJoinPenilaian();
      listPertanyaanByIdPenilaian = pertanyaanService.getPertanyaanByIdPenilaian(idPenilaian);

      SharedPreferences.getInstance().then((SharedPreferences prefs){
        print('jalankan');
        String idPenilaian = prefs.get('id_penilaian');
        String target_penilaian = prefs.get('target_penilaian');
        String objek_penilaian = prefs.get('objek_penilaian');
        String logo_objek_penilaian = prefs.get('logo_objek_penilaian');
        String skala = prefs.get('skala');
        String jenis_jawaban = prefs.get('jenis_jawaban');
        _currentPenilaian = Penilaian(id: idPenilaian, target_penilaian: target_penilaian, objek_penilaian: objek_penilaian, logo_objek_penilaian: logo_objek_penilaian, skala: skala, jenisJawaban: jenis_jawaban);
        
        String idPertanyaan = prefs.get('id_pertanyaan');
        String pertanyaan = prefs.get('pertanyaan');
        String pertanyaan_id_penilaian = prefs.get('pertanyaan_id_penilaian');
        
        _currentPertanyaan = Pertanyaan(id: idPertanyaan, pertanyaan: pertanyaan, penilaianId: pertanyaan_id_penilaian);
      });
      print('jalankan2');
    });
    
    SystemChrome.setEnabledSystemUIOverlays([]);    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: buildBody(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: _changeNavbarPage,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment, color: Colors.blueAccent,),
            title: Text('Penilaian')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.blueAccent,),
            title: Text('Home')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book, color: Colors.blueAccent,),
            title: Text('Pertanyaan')
          ),
        ],
      ),
    );
  }

  void _changeNavbarPage(int index){
    setState(() {
     _currentIndex = index;
    });
  }

  Widget buildBody(int index){
    
    if(index == 0){
      return penilaianPage();
    }else if(index == 1){
      return homePage();
    }else if(index == 2){
      return pertanyaanPage();
    }

  }

  Widget homePage(){
    return Container(
      child: CustomPaint(
        painter: DefaultPainter(),
        child: SafeArea(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Card(
                  color: Color(0x00000000),
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(
                      color: Colors.blueAccent,
                      width: 3
                    )
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(12),
                    child: Center(
                      child: Text('Mulai Kuesioner', style: TextStyle(fontSize: 22, color: Colors.blueAccent),),
                    ),
                  )
                ),
                Card(
                  color: Color(0x00000000),
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(
                      color: Colors.blueAccent,
                      width: 3
                    )
                  ),
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 20,),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Text('Pilih Penilaian', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                          ),
                          listPenilaian == null ? Text('') :

                          FutureBuilder(
                            future: listPenilaian,
                            builder: (BuildContext context, AsyncSnapshot<List<Penilaian>> snapshot) {

                                switch (snapshot.connectionState) {
                                  case ConnectionState.none:
                                    return Center(
                                      child: Text('Terjadi kesalahan koneksi'),
                                    );
                                  case ConnectionState.waiting:
                                    return Center(
                                      child: Text('Menunggu....'),
                                    );
                                  case ConnectionState.done:

                                    if(snapshot.hasData){
                                      return DropdownButtonFormField<Penilaian>(
                                        decoration: InputDecoration(
                                          contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                          fillColor: Colors.red,
                                        ),
                                        items: snapshot.data
                                            .map((penilaian) => DropdownMenuItem<Penilaian>(
                                                  child: Text(penilaian.objek_penilaian),
                                                  value: penilaian,
                                                ))
                                            .toList(),
                                        onChanged: (Penilaian value) {
                                          print('apa ini '+value.logo_objek_penilaian);
                                          setState(() {
                                            _currentPenilaian = value;
                                            
                                            listPertanyaanByIdPenilaian = pertanyaanService.getPertanyaanByIdPenilaian(_currentPenilaian.id);
                                          });
                                        },
                                        value: _currentPenilaian,
                                        validator: (Penilaian penilaian) => penilaian == null ? 'Penilaian belum di pilih' : null,
                                        hint: Text('Pilih Penilaian', style: TextStyle(color: Colors.black),),
                                      );
                                    }
                                    break;

                                  default :
                                } 

                            }
                          ),
                          
                          SizedBox(height: 20,),
                          Align(alignment: Alignment.bottomLeft,
                            child: Text("Pilih Pertanyaan", style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                          ),
                          _currentPenilaian == null ? Text('') : FutureBuilder(
                            future: listPertanyaanByIdPenilaian,
                            builder: (BuildContext context, AsyncSnapshot<List<Pertanyaan>> snapshot) {
                              
                              switch (snapshot.connectionState) {
                                case ConnectionState.none:
                                  return Center(
                                    child: Text('Terjadi kesalahan koneksi'),
                                  );
                                case ConnectionState.waiting:
                                  return Center(
                                      child: Text('Menunggu....'),
                                    );
                                case ConnectionState.done:

                                  if(snapshot.hasData){
                                    if(snapshot.data.length > 0){

                                      return DropdownButtonFormField<Pertanyaan>(
                                        decoration: InputDecoration(
                                          contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                          fillColor: Colors.red,
                                        ),
                                        items: snapshot.data
                                            .map((pertanyaan) => DropdownMenuItem<Pertanyaan>(
                                                  child: Text(pertanyaan.pertanyaan),
                                                  value: pertanyaan,
                                                ))
                                            .toList(),
                                        onChanged: (Pertanyaan value) {
                                          setState(() {
                                            _currentPertanyaan = value;
                                          });
                                        },
                                        //value: _currentPertanyaan != null ? _currentPertanyaan : null,
                                        validator: (Pertanyaan pertanyaan) => pertanyaan == null ? 'Pertanyaan belum di pilih' : null,
                                        hint: Text(_currentPertanyaan != null ? _currentPertanyaan.pertanyaan : '', style: TextStyle(color: Colors.black),),
                                      );

                                    }else{
                                      _currentPertanyaan = null;
                                      return DropdownButtonFormField<Pertanyaan>(
                                        decoration: InputDecoration(
                                          contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                          fillColor: Colors.red,
                                        ),
                                        items: [],
                                        onChanged: (Pertanyaan value) {
                                          
                                        },
                                        validator: (Pertanyaan pertanyaan) => pertanyaan == null ? 'Pertanyaan belum di pilih' : null,
                                        hint: Text('', style: TextStyle(color: Colors.black),),
                                      );  
                                    }
                                  }else{
                                    return Center(
                                      child: Text('Menunggu....'),
                                    );
                                  }
                                  break;

                                default : 
                                  return Center(
                                    child: Text('Menunggu....'),
                                  );
                              }
                            },  
                          ),
                          RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Text('Mulai Kuesioner', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                            onPressed: _mulaiKuesioner,
                            color: Colors.blueAccent,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }

  void _mulaiKuesioner(){
    if(_currentPenilaian != null && _currentPertanyaan != null){

      SharedPreferences.getInstance().then((SharedPreferences prefs){
        prefs.setBool('login', true);
        prefs.setString('id_penilaian', _currentPenilaian.id);
        prefs.setString('target_penilaian', _currentPenilaian.target_penilaian);
        prefs.setString('objek_penilaian', _currentPenilaian.objek_penilaian);
        prefs.setString('logo_objek_penilaian', _currentPenilaian.logo_objek_penilaian);
        prefs.setString('skala', _currentPenilaian.skala);
        prefs.setString('jenis_jawaban', _currentPenilaian.jenisJawaban);
        prefs.setString('id_pertanyaan', _currentPertanyaan.id);
        prefs.setString('pertanyaan', _currentPertanyaan.pertanyaan);
        prefs.setString('pertanyaan_id_penilaian', _currentPertanyaan.penilaianId);

        Navigator.of(context).pushNamedAndRemoveUntil('/kuesioner', (Route<dynamic> route) => false);
      });
    }else{
      Alert(context: context, title: "Kesalahan", desc: "Pilih Penilaian dan Pertanyaan dengan Benar").show();
    }
  }
  
  Widget penilaianPage(){
     return Container(
        child: CustomPaint(
          painter: PenilaianPainter(),
          child: SafeArea(
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10,),
                  Table(
                    columnWidths: {1 : FractionColumnWidth(0.8)},
                    children: [
                      TableRow(
                        children: [
                          Container(
                            width: 45,
                            height: 45,
                            child: FloatingActionButton(
                              backgroundColor: Colors.white,
                              child: Icon(Icons.add, color: Colors.black,),
                              onPressed: () => Navigator.of(context).pushNamed('/tambah_penilaian'),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10.0),
                            child: Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.all(
                                Radius.circular(30.0),
                              ),
                              child: TextField(
                                style: TextStyle(color: Colors.black, fontSize: 16.0),
                                onChanged: (input) => pencarian = input,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 14.0),
                                  suffixIcon: Material(
                                    elevation: 2.0,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(30.0),
                                    ),
                                    child: InkWell(
                                      onTap: () async {
                                          setState(() {
                                            listPenilaian = penilaianService.getSearchPenilaian(pencarian);  
                                          });
                                      },
                                      child: Icon(
                                        Icons.search,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ]
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Expanded(
                    child: FutureBuilder(
                      future: listPenilaian,
                      builder: (BuildContext context, AsyncSnapshot<List<Penilaian>> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                              return Center(
                                child: Text('Terjadi kesalahan koneksi'),
                              );
                            case ConnectionState.waiting:
                              return Center(
                                child: Text('Menunggu....'),
                              );
                            case ConnectionState.done:
                              if(snapshot.hasData){ 
                                return ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (BuildContext context, int index){
                                     return Card(
                                        elevation: 3,
                                        color: Colors.white,
                                        margin: EdgeInsets.only(left: 20, right: 20, top: 6, bottom: 0),
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 5, right: 5, bottom: 5, top: 2),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(bottom: 2),
                                                child: Align(
                                                  alignment: Alignment.centerRight,
                                                  child: GestureDetector(
                                                    child: Icon(Icons.more_horiz, color: Colors.black,),
                                                    onTap: (){
                                                      idPenilaianEditDelete = snapshot.data[index].id;
                                                      showDialog(
                                                        context: context,
                                                        builder: (BuildContext context) {
                                                          // return object of type Dialog
                                                          return AlertDialog(
                                                            title: new Text("Perintah"),
                                                            content: new Text("Pilih Ubah atau Hapus data ?"),
                                                            actions: <Widget>[
                                                              // usually buttons at the bottom of the dialog
                                                              new FlatButton(
                                                                child: new Text("Batal"),
                                                                onPressed: () => Navigator.of(context).pop(),
                                                              ),
                                                              new FlatButton(
                                                                child: new Text("Ubah"),
                                                                onPressed: () {
                                                                  ubah();
                                                                },
                                                              ),
                                                              new FlatButton(
                                                                child: new Text("Hapus"),
                                                                onPressed: () {
                                                                  hapus();
                                                                },
                                                              ),
                                                            ],
                                                          );
                                                        },
                                                      );
                                                    },
                                                  )
                                                ),
                                              ),
                                              Table(
                                                defaultColumnWidth: IntrinsicColumnWidth(),
                                                columnWidths: {1 : FractionColumnWidth(.6)},
                                                children: [
                                                  TableRow(
                                                    
                                                    children: [
                                                      Image.file(File(snapshot.data[index].logo_objek_penilaian), height: 100, alignment: Alignment.centerLeft,),
                                                      Align(
                                                        alignment: Alignment.centerLeft,
                                                        child: Column(
                                                          children: <Widget>[
                                                            SizedBox(height: 5,),
                                                            Align(
                                                              child: Text('Objek : '+snapshot.data[index].objek_penilaian, style: TextStyle(color: Colors.black87, fontSize: 18),),
                                                              alignment: Alignment.bottomLeft,
                                                            ),
                                                            SizedBox(height: 5,),
                                                            Align(
                                                              alignment: Alignment.bottomLeft,
                                                              child: Text('Target : '+snapshot.data[index].target_penilaian, style: TextStyle(color: Colors.black87, fontSize: 18),),
                                                            ),
                                                            SizedBox(height: 5,),
                                                            Align(
                                                              alignment: Alignment.bottomLeft,
                                                              child: Text('Skala : '+snapshot.data[index].skala, style: TextStyle(color: Colors.black87, fontSize: 18),),
                                                            ),
                                                            SizedBox(height: 5,),
                                                            Align(
                                                              alignment: Alignment.bottomLeft,
                                                              child: Text('Jenis : '+snapshot.data[index].jenisJawaban, style: TextStyle(color: Colors.black87, fontSize: 18),),
                                                            )
                                                          ],
                                                        ),
                                                      )
                                                    ]
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    
                                  },
                                );
                              }
                              break;

                            default :
                              return Center(
                                child: Text('Menunggu....'),
                              );
                          } 
                      }
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
  }

  ubah() async{
    
    await penilaianService.getPenilaianById(idPenilaianEditDelete).then((List<Penilaian> list){
      Penilaian penilaian;

      var row = list[0];
      penilaian = Penilaian(id : row.id, logo_objek_penilaian: row.logo_objek_penilaian, target_penilaian: row.target_penilaian, objek_penilaian: row.objek_penilaian, skala: row.skala, jenisJawaban: row.jenisJawaban);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => UbahPenilaianPage(
            penilaian: penilaian,
          )),
      );
    });

  }

  hapus() async{
    await penilaianService.delete(idPenilaianEditDelete);
    setState(() {
      listPenilaian = penilaianService.getAllPenilaian();
    });
  }
  Widget pertanyaanPage(){
    return Container(
        child: CustomPaint(
          painter: DefaultPainter(),
          child: SafeArea(
            child: Center(
              child: Column(
                children: <Widget>[
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text('Tambah Pertanyaan', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                    onPressed: (){
                      Navigator.of(context).pushNamed('/tambah_pertanyaan');
                    },
                    color: Colors.blueAccent,
                  ),
                  Expanded(
                    child: FutureBuilder(
                      future: listPertanyaan,
                      builder: (BuildContext context, AsyncSnapshot<List<Pertanyaan>> snapshot) {

                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                              return Center(
                                child: Text('Terjadi kesalahan koneksi'),
                              );
                            case ConnectionState.waiting:
                              return Center(
                                child: Text('Menunggu....'),
                              );
                            case ConnectionState.done:

                              if(snapshot.hasData){
                                
                                return ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (BuildContext context, int index){
                                    return Card(
                                      elevation: 3,
                                      color: Colors.white,
                                      margin: EdgeInsets.only(left: 20, right: 20, top: 6, bottom: 0),
                                      child: Padding(
                                        padding: EdgeInsets.all(5),
                                        child: Column(
                                          children: <Widget>[
                                            Align(
                                              child: Text('Objek Penilaian : '+snapshot.data[index].penilaian.objek_penilaian, style: TextStyle(color: Colors.blueAccent, fontSize: 18),),
                                              alignment: Alignment.bottomLeft,
                                            ),
                                            SizedBox(height: 5,),
                                            Align(
                                              child: Text('Target Penilaian : '+snapshot.data[index].penilaian.target_penilaian, style: TextStyle(color: Colors.blueAccent, fontSize: 18),),
                                              alignment: Alignment.bottomLeft,
                                            ),
                                            SizedBox(height: 5,),
                                            Align(
                                              child: Text('Pertanyaan : '+snapshot.data[index].pertanyaan, style: TextStyle(color: Colors.blueAccent, fontSize: 18),),
                                              alignment: Alignment.bottomLeft,
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }
                              break;

                            default :
                              return Center(
                                child: Text('Menunggu....'),
                              );
                          } 

                      }
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
  }
}