import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'painter/default_painter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  
  final _formKey = GlobalKey<FormState>();
  String ipAddress;
  bool isHost = true;
  String _pilihData;

  @override
  void initState() {
    _pilihData = '1';
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: CustomPaint(
          painter: DefaultPainter(),
          child: SafeArea(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  
                  Padding(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Text('Pilih Penyimpanan Data', style: TextStyle(fontSize: 18, color: Colors.blueAccent, fontWeight: FontWeight.bold),),
                        ),
                        SizedBox(height: 20,),
                        DropdownButtonFormField<String>(
                          decoration: InputDecoration(
                            contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                            fillColor: Colors.red,
                          ),
                          items: [
                            DropdownMenuItem(
                              value: '1',
                              child: Text('Data tersimpan online', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            DropdownMenuItem(
                              value: '2',
                              child: Text('Data berada di HP', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            )
                          ],
                          value: _pilihData,
                          onChanged: (value){
                            setState(() {
                            _pilihData = value; 
                            });
                          },
                        ),
                        _pilihData == '1' ? 
                        Column(
                          children: <Widget>[
                            SizedBox(height: 20,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Masukan Host', style: TextStyle(fontSize: 18, color: Colors.blueAccent, fontWeight: FontWeight.bold),),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                              ),
                              validator: (input) => input.length == 0 ? 'IP Address harus di isi' : null,
                              onSaved: (input) => ipAddress = input,
                            )
                          ],
                        )
                        :
                        SizedBox(),
                        
                        SizedBox(height: 20,),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text('Masuk', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                          onPressed: _masuk,
                          color: Colors.blueAccent,
                        ),
                      ],
                    ),
                  ),
                  )  
                ],
              ),
            ),
          ),
        ),
      )
    );
  }

  void _masuk() async{
    
    if(_pilihData == '1'){
      
      if(_formKey.currentState.validate()){
      
        _formKey.currentState.save();

        try {
          final response = await http.get(
            'http://'+ipAddress,
            headers: {'Accept' : 'application/json'}
            );
          
          if (response.statusCode == 200) {

            SharedPreferences.getInstance().then((SharedPreferences prefs){
              prefs.setString('ip_address', ipAddress);
              Navigator.of(context).pushNamed('/setup_pertanyaan');
            });

          } else {
            Alert(context: context, title: "Gagal", desc: "Ip Address Tidak Valid").show();
          }
        }catch(ex){
          Alert(context: context, title: "Gagal", desc: "Ip Address Tidak Valid").show();
        }
      }
      
    }else{
        Navigator.of(context).pushNamed('/setup_pertanyaan2');
    }

  }
}