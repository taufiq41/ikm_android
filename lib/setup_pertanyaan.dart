import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'model/penilaian.dart';
import 'model/pertanyaan.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SetupPertanyaan extends StatefulWidget {
  @override
  _SetupPertanyaanState createState() => _SetupPertanyaanState();
}

class _SetupPertanyaanState extends State<SetupPertanyaan> {
  
  Future<List<Penilaian>> futureListPenilaian;
  String ipAddress;

  @override
  void initState() {
    SharedPreferences.getInstance().then((SharedPreferences prefs){
      ipAddress = prefs.get('ip_address');
      setState(() {
        futureListPenilaian = getPenilaian();    
      });
    });
    super.initState();
    
  }

  final _formKey = GlobalKey<FormState>();
  String _target_pertanyaan, _objek_pertanyaan, _pertanyaan;
  //Penilaian _currentPenilaian = Penilaian(id : '1', target_penilaian :  'm', objek_penilaian : '1', logo_objek_penilaian : 'ok');
  Penilaian _currentPenilaian;
  Pertanyaan _currentPertanyaan;

  Future<List<Penilaian>> getPenilaian() async{
    try {
        final response = await http.get(
          'http://'+ipAddress+'/ikm/api/get_penilaian.php',
          headers: {'Accept' : 'application/json'}
          );
        if (response.statusCode == 200) {
           
           List items = json.decode(response.body);

           List<Penilaian>  listPenilaian = List();
           
           
           for(var row in items){
              Map<String, dynamic> json = row;
              listPenilaian.add(Penilaian.fromJson(json));
           }
           return listPenilaian;
           
        } else {
          print('error');
          return null;
        }
      } catch (ex) {
        print('kesalahan 2'+ex.toString());
        return null;
      }

  }

  Future<List<Pertanyaan>> getPertanyaan() async{
    var jsonEncode = json.encode({'penilaian_id' : _currentPenilaian.id});
    try {
        final response = await http.post(
          'http://'+ipAddress+'/ikm/api/get_pertanyaan.php',
          body : jsonEncode,
          headers: {'Accept' : 'application/json'}
          );
        if (response.statusCode == 200) {
           
           List items = json.decode(response.body);

           List<Pertanyaan> listPertanyaan = List();
   
           for(var row in items){
              Map<String, dynamic> json = row;
              listPertanyaan.add(Pertanyaan.fromJson(json));           
           }
           return listPertanyaan;
           
        } else {
          print('error');
          return null;
        }
      } catch (ex) {
        
        print('kesalahan '+ex.toString());
        return null;
      }
  }

  @override
  Widget build(BuildContext context) {
    getPenilaian();
    return Container(
      child: Card(
        child : Center(
          child: Padding(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Setup Pertanyaan', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.blueAccent),),
                SizedBox(height: 30,),

                Align(alignment: Alignment.bottomLeft,
                  child: Text("Pilih Penilaian", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                ),
                FutureBuilder(
                  future: futureListPenilaian,
                  builder: (BuildContext context, AsyncSnapshot<List<Penilaian>> snapshot) {
                    
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return Center(
                          child: Text('Terjadi kesalahan koneksi'),
                        );
                      case ConnectionState.waiting:
                      case ConnectionState.done:

                        if(snapshot.hasData){
                          return DropdownButton<Penilaian>(
                            items: snapshot.data
                                .map((penilaian) => DropdownMenuItem<Penilaian>(
                                      child: Text(penilaian.objek_penilaian),
                                      value: penilaian,
                                    ))
                                .toList(),
                            onChanged: (Penilaian value) {
                              setState(() {
                                _currentPenilaian = value;
                              });
                            },
                            isExpanded: true, 
                            //value: Penilaian.fromJson({'id' : '1', 'target_penilaian' :  'm', 'objek_penilaian' : '1', 'logo_objek_penilaian' : 'ok'}),
                            //value: Penilaian(id : '1', target_penilaian :  'm', objek_penilaian : '1', logo_objek_penilaian : 'ok'),
                            //value : snapshot.data[2],
                            //value: _currentPenilaian,
                            hint: Text(_currentPenilaian != null ? _currentPenilaian.objek_penilaian : '', style: TextStyle(color: Colors.black),),
                          );
                        }else{
                          return Center(
                            child: CircularProgressIndicator()
                          );
                        }
                        break;
                      default :
                    }
                  },  
                ),
                SizedBox(height: 20,),
                Align(alignment: Alignment.bottomLeft,
                  child: Text("Pilih Pertanyaan", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                ),
                _currentPenilaian == null ? Text('') : FutureBuilder(
                  future: getPertanyaan(),
                  builder: (BuildContext context, AsyncSnapshot<List<Pertanyaan>> snapshot) {
                    
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return Center(
                          child: Text('Terjadi kesalahan koneksi'),
                        );
                      case ConnectionState.waiting:
                      case ConnectionState.done:

                        if(snapshot.hasData){
                          return DropdownButton<Pertanyaan>(
                            items: snapshot.data
                                .map((pertanyaan) => DropdownMenuItem<Pertanyaan>(
                                      child: Text(pertanyaan.pertanyaan),
                                      value: pertanyaan,
                                    ))
                                .toList(),
                            onChanged: (Pertanyaan value) {
                              setState(() {
                                _currentPertanyaan = value;
                              });
                            },
                            isExpanded: true, 
                            //value: _currentPertanyaan,
                            hint: Text(_currentPertanyaan != null ? _currentPertanyaan.pertanyaan : '', style: TextStyle(color: Colors.black),),
                          );
                        }else{
                          return Center(
                            child: CircularProgressIndicator()
                          );
                        }
                        break;
                      default :
                    }
                  },  
                ),
                
                SizedBox(height: 20,),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Text('Lanjut', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                  onPressed: _submit,
                  color: Colors.blueAccent,
                ),
              ],
            ),
          )

        )
      )
    );
  }

  void _submit() async{

    if(_currentPenilaian != null && _currentPertanyaan != null){

      SharedPreferences.getInstance().then((SharedPreferences prefs){
        prefs.setBool('login', true);
        prefs.setString('id_penilaian', _currentPenilaian.id);
        prefs.setString('target_penilaian', _currentPenilaian.target_penilaian);
        prefs.setString('objek_penilaian', _currentPenilaian.objek_penilaian);
        prefs.setString('logo_objek_penilaian', _currentPenilaian.logo_objek_penilaian);
        prefs.setString('skala', _currentPenilaian.skala);
        prefs.setString('jenis_jawaban', _currentPenilaian.jenisJawaban);
        prefs.setString('id_pertanyaan', _currentPertanyaan.id);
        prefs.setString('pertanyaan', _currentPertanyaan.pertanyaan);

        Navigator.of(context).pushNamedAndRemoveUntil('/penilaian', (Route<dynamic> route) => false);
      });
    }else{
      Alert(context: context, title: "Kesalahan", desc: "Pilih Penilaian dan Pertanyaan dengan Benar").show();
    }
  }
}