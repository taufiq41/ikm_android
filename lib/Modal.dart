import 'package:flutter/material.dart';
import 'database/penilaian_service.dart';

class Modal{
  String penilaianId;
  PenilaianService penilaianService;
  BuildContext context2;

  Modal(){
    penilaianService = PenilaianService();
  }
  mainBottomSheet(BuildContext context){
    context2 = context;
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context){
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _createTile(context, 'Ubah', Icons.edit, _action1),
              _createTile(context, 'Hapus', Icons.delete, _action2),
            ],
          );
        }
    );
  }

  ListTile _createTile(BuildContext context, String name, IconData icon, Function action){
    return ListTile(
      leading: Icon(icon),
      title: Text(name),
      onTap: (){
        Navigator.pop(context);
        action();
      },
    );
  }

  _action1(){
    print('action 1');
  }

  _action2() async{
    await penilaianService.delete(penilaianId);
  }

}