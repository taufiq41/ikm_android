import 'package:flutter/material.dart';
import 'home_skala5.dart';
import 'setup_pertanyaan.dart';
import 'charts.dart';
import 'login.dart';
import 'setup_pertanyaan_offline.dart';
import 'pages/tambah_penilaian.dart';
import 'pages/tambah_pertanyaan.dart';
import 'kuesioner.dart';
import 'charts2.dart';
import 'package:flutter/services.dart';
import 'database/database_provider.dart';
import 'pages/edit_penilaian.dart';

void main() async{
  await DBProvider().initDatabase();
  SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(MyApp());
}

  
class MyApp extends StatelessWidget {  

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SetupPertanyaanOffline(),
      routes: <String, WidgetBuilder>{
        '/login' : (BuildContext context) => LoginPage(),
        '/setup_pertanyaan' : (BuildContext context) => SetupPertanyaan(),
        '/penilaian' : (BuildContext context) => HomeSkala5(),
        '/charts' : (BuildContext context) => Charts(),
        '/setup_pertanyaan2' : (BuildContext context) => SetupPertanyaanOffline(),
        '/tambah_penilaian' : (BuildContext context) => TambahPenilaianPage(),
        '/tambah_pertanyaan' : (BuildContext context) => TambahPertanyaanPage(),
        '/kuesioner' : (BuildContext context) => KuesionerPage(),
        '/charts2' : (BuildContext context) => Charts2(),
        '/edit_penilaian' : (BuildContext context) => UbahPenilaianPage(),
      },
      debugShowCheckedModeBanner: false
    );
  }
}
