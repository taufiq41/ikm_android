import 'package:sqflite/sqflite.dart';
import 'database_provider.dart';
import '../model/pertanyaan.dart';
import '../model/penilaian.dart';

class PertanyaanService{


  DBProvider _dbProvider;
  Database _db;

  PertanyaanService(){
    _dbProvider = DBProvider();
    initDatabase();
  }

  initDatabase() async{
    _db = await _dbProvider.db;
  }

  Future<List<Pertanyaan>> getAllPertanyaan() async{
    _db = await _dbProvider.db;
    List<Map> maps = await _db.query('pertanyaan', columns: ['id', 'penilaian_id', 'pertanyaan']);
    List<Pertanyaan> pertanyaan = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        print(maps[i]);
        pertanyaan.add(Pertanyaan.fromJson(maps[i]));
      }
    }
    return pertanyaan;
  }

  Future<List<Pertanyaan>> getAllPertanyaanJoinPenilaian() async{
    _db = await _dbProvider.db;

    List<Pertanyaan> listPertanyaan = [];
    List<Map> list = await _db.rawQuery('SELECT penilaian.*, pertanyaan.id as pertanyaan_id, pertanyaan.pertanyaan FROM pertanyaan INNER JOIN penilaian ON pertanyaan.penilaian_id=penilaian.id');

    if (list.length > 0) {
        for (int i = 0; i < list.length; i++) {
         
          /* print(list[i]['id']);
          print(list[i]['target_penilaian']);
          print(list[i]['objek_penilaian']);
          print(list[i]['logo_objek_penilaian']);
          print(list[i]['skala']);
          print(list[i]['jenis_jawaban']);
          print(list[i]['pertanyaan_id']);
          print(list[i]['pertanyaan']); */
          Penilaian penlaian = Penilaian(id: list[i]['id'].toString(), target_penilaian: list[i]['target_penilaian'], objek_penilaian: list[i]['objek_penilaian'], skala: list[i]['skala'].toString(), jenisJawaban: list[i]['jenis_jawaban']);
          Pertanyaan pertanyaan = Pertanyaan(id: list[i]['pertanyaan_id'].toString(), pertanyaan: list[i]['pertanyaan']);
          pertanyaan.penilaian = penlaian;
          
          listPertanyaan.add(pertanyaan);
        }
      }
    
    return listPertanyaan;
  }

  Future<List<Pertanyaan>> getPertanyaanByIdPenilaian(String idPenilaian) async{
    _db = await _dbProvider.db;

    List<Pertanyaan> listPertanyaan = [];
    List<Map> list = await _db.rawQuery('SELECT penilaian.*, pertanyaan.id as pertanyaan_id, pertanyaan.pertanyaan FROM pertanyaan INNER JOIN penilaian ON pertanyaan.penilaian_id=penilaian.id WHERE penilaian.id='+idPenilaian);

    if (list.length > 0) {
        for (int i = 0; i < list.length; i++) {
         
          /* print(list[i]['id']);
          print(list[i]['target_penilaian']);
          print(list[i]['objek_penilaian']);
          print(list[i]['logo_objek_penilaian']);
          print(list[i]['skala']);
          print(list[i]['jenis_jawaban']);
          print(list[i]['pertanyaan_id']);
          print(list[i]['pertanyaan']); */
          Penilaian penlaian = Penilaian(id: list[i]['id'].toString(), target_penilaian: list[i]['target_penilaian'], objek_penilaian: list[i]['objek_penilaian'], skala: list[i]['skala'].toString(), jenisJawaban: list[i]['jenis_jawaban']);
          Pertanyaan pertanyaan = Pertanyaan(id: list[i]['pertanyaan_id'].toString(), pertanyaan: list[i]['pertanyaan']);
          pertanyaan.penilaian = penlaian;
          
          listPertanyaan.add(pertanyaan);
        }
      }
    
    return listPertanyaan;
  }

  Future<Pertanyaan> save(Pertanyaan pertanyaan) async {
    _db = await _dbProvider.db;
    await _db.insert('pertanyaan', pertanyaan.toMap()).then((int value){
      print('berhasil insert dengan id '+value.toString());
      pertanyaan.id = value.toString();
      return pertanyaan;
    });
    
    /*
    await dbClient.transaction((txn) async {
      var query = "INSERT INTO $TABLE ($NAME) VALUES ('" + employee.name + "')";
      return await txn.rawInsert(query);
    });
    */
  }
}