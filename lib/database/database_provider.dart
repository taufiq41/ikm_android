import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {

  Database _db;

  initDatabase() async {
    final path = await getDatabasePath('ikm');
    _db = await openDatabase(path, version: 1, onCreate: onCreate);
    return _db;
  }

  Future<Database> get db async{
    if(_db != null){
      return _db;
    }
    _db = await initDatabase();
    return _db;
  }

  static void databaseLog(String functionName, String sql,
      [List<Map<String, dynamic>> selectQueryResult, int insertAndUpdateQueryResult, List<dynamic> params]) {
    print(functionName);
    print(sql);
    if (params != null) {
      print(params);
    }
    if (selectQueryResult != null) {
      print(selectQueryResult);
    } else if (insertAndUpdateQueryResult != null) {
      print(insertAndUpdateQueryResult);
    }
  }

  Future<void> createTabelPenilaian(Database db) async {
    print('dijalankan');
    final tabelPenilaian = '''CREATE TABLE penilaian
    (
      id INTEGER PRIMARY KEY,
      target_penilaian TEXT,
      objek_penilaian TEXT,
      logo_objek_penilaian TEXT,
      skala INTEGER NOT NULL,
      jenis_jawaban TEXT NOT NULL
    )''';

    await db.execute(tabelPenilaian);

    final tabelPertanyaan = '''CREATE TABLE pertanyaan
    (
      id INTEGER PRIMARY KEY,
      penilaian_id INTEGER NOT NULL,
      pertanyaan TEXT NOT NULL
    )''';

    await db.execute(tabelPertanyaan);

    final tabelJawaban = '''CREATE TABLE jawaban
    (
      id INTEGER PRIMARY KEY,
      pertanyaan_id INTEGER NOT NULL,
      nilai INTEGER NOT NULL
    )''';

    await db.execute(tabelJawaban);
    
  }

  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);

    //make sure the folder exists
    if (await Directory(dirname(path)).exists()) {
      //await deleteDatabase(path);
    } else {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }

  Future<void> onCreate(Database db, int version) async {
    await createTabelPenilaian(db);
  }

}