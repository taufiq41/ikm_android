import 'package:sqflite/sqflite.dart';
import 'database_provider.dart';
import '../model/jawaban.dart';

class JawabanService{

  DBProvider _dbProvider;
  Database _db;

  JawabanService(){
    _dbProvider = DBProvider();
    initDatabase();
  }

  initDatabase() async{
    _db = await _dbProvider.db;
  }

  Future<Map<String, dynamic>> getJawabanById({String idPertanyaan = '0'}) async{
    /* _db = await _dbProvider.db; */
    List<Map> maps = await _db.rawQuery('SELECT SUM(CASE WHEN nilai = 5 THEN 1 ELSE 0 END) as lima, SUM(CASE WHEN nilai = 4 THEN 1 ELSE 0 END) as empat, SUM(CASE WHEN nilai = 3 THEN 1 ELSE 0 END) as tiga, SUM(CASE WHEN nilai = 2 THEN 1 ELSE 0 END) as dua, SUM(CASE WHEN nilai = 1 THEN 1 ELSE 0 END) as satu, SUM(CASE WHEN nilai = 0 THEN 1 ELSE 0 END) as nol FROM jawaban WHERE pertanyaan_id='+idPertanyaan);
    
    Map<String, dynamic> listJawaban;

    if (maps.length > 0) {
     
        listJawaban = <String, dynamic>{
          'nol' : maps[0]['nol'].toString(),
          'satu' : maps[0]['satu'].toString(),
          'dua' : maps[0]['dua'].toString(),
          'tiga' : maps[0]['tiga'].toString(),
          'empat' : maps[0]['empat'].toString(),
          'lima' : maps[0]['lima'].toString()
        };

        /* Jawaban jawaban = Jawaban(id: maps[i]['id'], idPertanyaan: maps[i]['pertanyaan_id'], nilai: maps[i]['nilai'],);
        listJawaban.add(jawaban); */
      
    }else{
      listJawaban = <String, dynamic>{
        'nol' : '0',
        'satu' : '1',
        'dua' : '2',
        'tiga' : '3',
        'empat' : '4',
        'lima' : '5'
      };
    }
    return listJawaban;
  }

  Future<Jawaban> save(Jawaban jawaban) async {
    _db = await _dbProvider.db;
    await _db.insert('jawaban', jawaban.toMap()).then((int value){
      jawaban.id = value.toString();
      return jawaban;
    });
  }

}