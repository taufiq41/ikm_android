import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'database_provider.dart';
import '../model/penilaian.dart';

class PenilaianService{


  DBProvider _dbProvider;
  Database _db;

  PenilaianService(){
    _dbProvider = DBProvider();
    initDatabase();
  }

  initDatabase() async{
    _db = await _dbProvider.db;
  }

  Future<List<Penilaian>> getAllPenilaian() async{
    _db = await _dbProvider.db;
    List<Map> maps = await _db.query('penilaian', columns: ['id', 'target_penilaian', 'objek_penilaian', 'skala', 'jenis_jawaban','logo_objek_penilaian']);
    List<Penilaian> penilaian = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        print(maps[i]);
        penilaian.add(Penilaian.fromJson(maps[i]));
      }
    }
    return penilaian;
  }

  Future<List<Penilaian>> getPenilaianById(String id) async{
    _db = await _dbProvider.db;
    List<Map> maps = await _db.rawQuery('SELECT * FROM penilaian WHERE id=?', [id]);
    List<Penilaian> penilaian = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        print(maps[i]);
        penilaian.add(Penilaian.fromJson(maps[i]));
      }
    }
    return penilaian;
  }

  Future<List<Penilaian>> getSearchPenilaian(String pencarian) async{
    _db = await _dbProvider.db;
    List<Map> maps = await _db.rawQuery('SELECT * FROM penilaian WHERE target_penilaian LIKE ? OR objek_penilaian LIKE ? ', ['%'+pencarian+'%','%'+pencarian+'%']);
    List<Penilaian> penilaian = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        print(maps[i]);
        penilaian.add(Penilaian.fromJson(maps[i]));
      }
    }
    return penilaian;
  }

  Future<Penilaian> save(Penilaian penilaian) async {
    var dbClient = await _db;
    await dbClient.insert('penilaian', penilaian.toMap()).then((int value){
      print('berhasil insert dengan id '+value.toString());
      penilaian.id = value.toString();
      return penilaian;
    });
    
    /*
    await dbClient.transaction((txn) async {
      var query = "INSERT INTO $TABLE ($NAME) VALUES ('" + employee.name + "')";
      return await txn.rawInsert(query);
    });
    */
  }
  Future delete(String penilianId) async{
    var dbClient = await _db;
    int penilaianId2 = int.parse(penilianId);
    await dbClient.delete("penilaian", where: "id = ?", whereArgs: [penilaianId2]).then((int value){
      print('berhasil delete '+value.toString());
    });
  }

  Future edit(Penilaian penilaian) async{
    var dbClient = await _db;
    await dbClient.update('penilaian', penilaian.toMap(), where: "id = ?", whereArgs: [penilaian.id]).then((int result){
      print('Selesai Update');
    });
  }
}