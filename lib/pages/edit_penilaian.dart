import 'package:flutter/material.dart';
import 'package:indek_kepuasan_masyarakat/painter/default_painter.dart';
import 'package:path_provider/path_provider.dart';
import '../model/penilaian.dart';
import '../database/penilaian_service.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class UbahPenilaianPage extends StatefulWidget {
  Penilaian penilaian;

  UbahPenilaianPage({this.penilaian});
  
  @override
  _UbahPenilaianPageState createState() => _UbahPenilaianPageState(penilaian: penilaian);
}

class _UbahPenilaianPageState extends State<UbahPenilaianPage> {
  Penilaian penilaian;
  
  _UbahPenilaianPageState({this.penilaian});

  final _formKey = GlobalKey<FormState>();
  String _pilihData;
  String _id, _target_penilaian, _objek_penilaian, _skala, _jenis_jawaban, _logo_objek_penilaian, _logo_objek_penilaian2;
  PenilaianService penilaianService;
  File galleryFile;

  @override
  void initState() {
    penilaianService = PenilaianService();
    _id = penilaian.id;
    _jenis_jawaban = penilaian.jenisJawaban;
    _skala = penilaian.skala;
    _target_penilaian = penilaian.target_penilaian;
    _objek_penilaian = penilaian.objek_penilaian;
    _logo_objek_penilaian = penilaian.logo_objek_penilaian;
    _logo_objek_penilaian2 = penilaian.logo_objek_penilaian;
    galleryFile = File(_logo_objek_penilaian);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    _selectImageGallery() async{
      await ImagePicker.pickImage(
        source: ImageSource.gallery,
      ).then((File file) async{
        if(file != null){
          file = await ImageCropper.cropImage(
            sourcePath: file.path,
            ratioX: 1.0,
            ratioY: 1.0,
            maxWidth: 100,
            maxHeight: 100,
          );

          galleryFile = file;
          
          await FlutterImageCompress.compressAndGetFile(
            file.path,
            file.path,
            quality: 20,
          );

          Directory directory = await getExternalStorageDirectory();
          DateTime now = new DateTime.now();
          String tanggal = now.day.toString();
          String bulan = now.month.toString();
          String tahun = now.year.toString();
          String jam = now.hour.toString();
          String menit = now.minute.toString();
          String detik = now.second.toString();
          String milidetik = now.millisecond.toString();

          List splitPath = file.path.split('/');
          String namafile = splitPath[splitPath.length-1];
          
          List splitExtension = namafile.split('.');
          String extensionFile = splitExtension[splitExtension.length-1];
          
          _logo_objek_penilaian = directory.path+'/'+tanggal+bulan+tahun+jam+menit+detik+milidetik+'.'+extensionFile;
          print(_logo_objek_penilaian);
        }else{

        }
      });

    }
    
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: CustomPaint(
          painter: DefaultPainter(),
          child: SafeArea(
            child: Container(
              child : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                    color: Colors.white,
                    /* color: Color(0x00000000), */
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Colors.blueAccent,
                        width: 3
                      )
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Text('Menu Ubah Penilaian', style: TextStyle(fontSize: 22, color: Colors.blueAccent),),
                      ),
                    )
                  ),
                  Card(
                    color: Colors.white,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Colors.blueAccent,
                        width: 3
                      )
                    ),
                    child: Form(
                      key: _formKey,
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 10,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Target Penilaian', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                              ),
                              validator: (input) => input.length == 0 ? 'Target Penilaian harus di isi' : null,
                              onSaved: (input) => _target_penilaian = input,
                              initialValue: _target_penilaian,
                            ),
                            SizedBox(height: 10,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Objek Yang Di Nilai', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                              ),
                              validator: (input) => input.length == 0 ? 'Objek Penilaian harus di isi' : null,
                              onSaved: (input) => _objek_penilaian = input,
                              initialValue: _objek_penilaian,
                            ),
                            SizedBox(height: 20,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Skala Penilaian', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            DropdownButtonFormField<String>(
                              decoration: InputDecoration(
                                contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                fillColor: Colors.red,
                              ),
                              items: [
                                DropdownMenuItem(
                                  value: '5',
                                  child: Text('Lima (5)', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                ),
                                DropdownMenuItem(
                                  value: '3',
                                  child: Text('Tiga (3)', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                ),
                                DropdownMenuItem(
                                  value: '2',
                                  child: Text('Dua (2)', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                )
                              ],
                              value: _skala,
                              onChanged: (value){
                                setState(() {
                                _skala = value; 
                                });
                              },
                            ),
                            SizedBox(height: 20,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Jenis Jawaban', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            DropdownButtonFormField<String>(
                              decoration: InputDecoration(
                                contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                fillColor: Colors.red,
                              ),
                              items: [
                                DropdownMenuItem(
                                  value: 'Puas',
                                  child: Text('Puas', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                ),
                                DropdownMenuItem(
                                  value: 'Penting',
                                  child: Text('Penting', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                ),
                                DropdownMenuItem(
                                  value: 'Setuju',
                                  child: Text('Setuju', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                ),
                                DropdownMenuItem(
                                  value: 'Baik',
                                  child: Text('Baik', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                ),
                                DropdownMenuItem(
                                  value: 'Hebat',
                                  child: Text('Hebat', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                                )
                              ],
                              value: _jenis_jawaban,
                              onChanged: (value){
                                setState(() {
                                _jenis_jawaban = value; 
                                });
                              },
                            ),
                            SizedBox(height: 20,),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                                side: BorderSide(color: Colors.blueAccent, width: 2)
                              ),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.image, size: 50,),
                                  Text('Pilih logo gambar'),
                                ],
                              ),
                              onPressed: _selectImageGallery,
                            ),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Text('Ubah Penilaian', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                              onPressed: _ubah,
                              color: Colors.blueAccent,
                            ),
                          ],
                        ),
                      )
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _ubah() async{
    if(_formKey.currentState.validate()){ 
        _formKey.currentState.save();
        Penilaian penilaian = Penilaian(id: _id, target_penilaian: _target_penilaian, objek_penilaian: _objek_penilaian, skala: _skala, jenisJawaban: _jenis_jawaban, logo_objek_penilaian: _logo_objek_penilaian);
        await penilaianService.edit(penilaian).then((Object penilaian){
          if(_logo_objek_penilaian == _logo_objek_penilaian2){
            print('logo tidak diganti');
          }else{
            galleryFile.copy(_logo_objek_penilaian).then((File file){
              print('selesai copy gambar');
            });
          }
          Navigator.of(context).pushNamedAndRemoveUntil('/setup_pertanyaan2', (Route<dynamic> route) => false);
        });

        
    }
  }
}