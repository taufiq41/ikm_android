import 'package:flutter/material.dart';
import 'package:indek_kepuasan_masyarakat/painter/default_painter.dart';
import '../model/penilaian.dart';
import '../model/pertanyaan.dart';
import '../database/penilaian_service.dart';
import '../database/pertanyaan_service.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class TambahPertanyaanPage extends StatefulWidget {
  @override
  _TambahPertanyaanPageState createState() => _TambahPertanyaanPageState();
}

class _TambahPertanyaanPageState extends State<TambahPertanyaanPage> {
  
  String _pertanyaan;
  Penilaian _currentPenilaian;
  Future<List<Penilaian>> futureListPenilaian;
  PenilaianService penilaianService;
  PertanyaanService pertanyaanService;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    penilaianService = PenilaianService();
    pertanyaanService = PertanyaanService();
    futureListPenilaian = penilaianService.getAllPenilaian();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: CustomPaint(
          painter: DefaultPainter(),
          child: SafeArea(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Card(
                    color: Color(0x00000000),
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Colors.blueAccent,
                        width: 3
                      )
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(12),
                      child: Center(
                        child: Text('Menu Tambah Pertanyaan', style: TextStyle(fontSize: 22, color: Colors.blueAccent),),
                      ),
                    )
                  ),
                  Card(
                    color: Color(0x00000000),
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Colors.blueAccent,
                        width: 3
                      )
                    ),
                    child: Form(
                      key: _formKey,
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 20,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Pilih Penilaian', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            FutureBuilder(
                              future: futureListPenilaian,
                              builder: (BuildContext context, AsyncSnapshot<List<Penilaian>> snapshot) {

                                  switch (snapshot.connectionState) {
                                    case ConnectionState.none:
                                      return Center(
                                        child: Text('Terjadi kesalahan koneksi'),
                                      );
                                    case ConnectionState.waiting:
                                      return Center(
                                        child: Text('Menunggu....'),
                                      );
                                    case ConnectionState.done:

                                      if(snapshot.hasData){
                                        print('panjang '+snapshot.data.length.toString());
                                        return DropdownButtonFormField<Penilaian>(
                                          decoration: InputDecoration(
                                            contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                            enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                            fillColor: Colors.red,
                                          ),
                                          items: snapshot.data
                                              .map((penilaian) => DropdownMenuItem<Penilaian>(
                                                    child: Text(penilaian.objek_penilaian),
                                                    value: penilaian,
                                                  ))
                                              .toList(),
                                          onChanged: (Penilaian value) {
                                            setState(() {
                                              _currentPenilaian = value;
                                            });
                                          },
                                          validator: (Penilaian penilaian) => penilaian == null ? 'Penilaian belum di pilih' : null,
                                          //value: Penilaian.fromJson({'id' : '1', 'target_penilaian' :  'm', 'objek_penilaian' : '1', 'logo_objek_penilaian' : 'ok'}),
                                          //value: Penilaian(id : '1', target_penilaian :  'm', objek_penilaian : '1', logo_objek_penilaian : 'ok'),
                                          //value : snapshot.data[2],
                                          //value: _currentPenilaian,
                                          hint: Text(_currentPenilaian != null ? _currentPenilaian.objek_penilaian : '', style: TextStyle(color: Colors.black),),
                                        );
                                      }
                                      break;

                                    default :
                                      return Center(
                                        child: Text('Menunggu....'),
                                      );
                                  } 

                              }
                            ),
                            SizedBox(height: 20,),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Text('Pertanyaan', style: TextStyle(fontSize: 18, color: Colors.blueAccent),),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                contentPadding: new EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                                enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.blueAccent, width: 2)),
                              ),
                              validator: (input) => input.length == 0 ? 'Pertanyaan harus di isi' : null,
                              onSaved: (input) => _pertanyaan = input,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Text('Tambah Penilaian', style: TextStyle(fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold),),
                              onPressed: _tambah,
                              color: Colors.blueAccent,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ),
      
    );
  }

  void _tambah() async{
    if(_formKey.currentState.validate()){ 
        _formKey.currentState.save();
        
        Pertanyaan pertanyaan = Pertanyaan(id: null, pertanyaan: _pertanyaan, penilaianId: _currentPenilaian.id);
        pertanyaanService.save(pertanyaan).then((Pertanyaan pertanyaan){
          Alert(
            context: context,
            type: AlertType.success,
            title: "Berhasil Tambah",
            desc: "Aplikasi berhasil menambah data pertanyaan",
            buttons: [
              DialogButton(
                child: Text(
                  "OK",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil('/setup_pertanyaan2', (Route<dynamic> route) => false),
                width: 120,
              )
            ],
          ).show();
        });
    }
  }
}